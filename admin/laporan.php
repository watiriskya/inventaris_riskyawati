<?php
include('cek.php');
error_reporting(0);
?>
<?php
include('cek_level.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Laporan | Admin</title>

        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- DataTables CSS -->
        <link href="../css/dataTables/dataTables.bootstrap.css" rel="stylesheet">

        <!-- DataTables Responsive CSS -->
        <link href="../css/dataTables/dataTables.responsive.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">
		
		<link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header">
                    <a class="navbar-brand" href="inventaris.php"><font color="white">Inventaris Sarana dan Prasarana di SMK</font></a>
                </div>

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>


                <ul class="nav navbar-right navbar-top-links">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <font color="white"><i class="fa fa-user fa-fw"></i> <?php echo $_SESSION['nama_petugas'];?>  <b class="caret"></b></font>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="profile.php"><i class="fa fa-user fa-fw"></i> Profile</a></li>
                            <li><a href="ubah_password.php"><i class="fa fa-key fa-fw"></i> Ubah Password</a></li>
                            <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
                <!-- /.navbar-top-links -->
          <?php
            
			if ($_SESSION['id_level']==1){
                echo'<div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form"><br/>
                                     <tr>
										<td colspan="3">&nbsp;&nbsp;<IMG width="200" height="200" SRC="images/user.png"></td>
									</tr>
                                </div><br/>
                                <!-- /input-group -->
                            </li>
							<li>
                                <a href="dashboard.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                            </li>
                            <li>
                                <a href="inventaris.php"><i class="fa fa-mail-forward fa-fw"></i> Inventarisir</a>
                            </li>
							<li>
                                <a href="peminjaman.php"><i class="fa fa-edit fa-fw"></i> Peminjaman</a>
                            </li>
							<li>
                                <a href="pengembalian.php"><i class="fa fa-arrow-left fa-fw"></i> Pengembalian</a>
                            </li>
                           <li>
                                <a href="#"><i class="fa fa-gear fa-fw"></i> Lainnya<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="petugas.php">Petugas</a>
                                    </li>
									<li>
                                        <a href="laporan.php">Generate Laporan</a>
                                    </li>
									<li>
                                        <a href="logout.php">Logout</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <!---/penutup--->';
			}
			?>
				<div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header" align="center">Laporan</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row --><br/>
                 <div class="row">
				 <div class="col-lg-1 col-md-3"></div>
				 <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                            <div class="row"><br/>
                                    <center><div><h3>Laporan Pertanggal</h3></div></center>
									<br/>
                            </div>
                        <a href="laporan_tanggal.php">
                            <div class="panel-footer">
                                <span class="pull-left">Selengkapnya</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
				<div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                            <div class="row"><br/>
                                    <center><div><h3>Laporan Perbulan</h3></div></center>
									<br/>
                            </div>
                        <a href="laporan_bulan.php">
                            <div class="panel-footer">
                                <span class="pull-left">Selengkapnya</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
				<div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                            <div class="row"><br/>
                                    <center><div><h3>Laporan Pertahun</h3></div></center>
									<br/>
                            </div>
                        <a href="laporan_tahun.php">
                            <div class="panel-footer">
                                <span class="pull-left">Selengkapnya</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>		
				</div><br/><br/>				
            <!-- /.row -->
						<div class="pull-right">
							<a href="inventaris.php"><button type="button" class="btn btn-outline btn-danger fa fa-reply "> Back To Inventarisir </button></a></p>
                          </div>
                    <div class="row">
                        <div class="col-lg-12">
							<div class="panel panel-default">
                                <div class="panel-heading">
                                    Laporan Keseluruhan 
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
								
						 <div class="box-body">
            <form method="POST" action="laporan.php">
            Mulai tanggal <input type="date" name="tanggal_awal"/>
            Akhir tanggal <input type="date" name="tanggal_akhir"/>
            <input type="submit" value="cari" name="pencarian" class="btn btn-primary">
            </form>
            </br>
                                        <table id="example" class="table table-striped table-bordered table-hover" >
                                            <thead>
											<?php
									include "koneksi.php";
                                    $no = 1;
                                    //proses jika sudah klik tombol pencarian data
                                    if(isset($_POST['pencarian'])){
                                    //menangkap nilai form
                                    $tanggal_awal=$_POST['tanggal_awal'];
                                    $tanggal_akhir=$_POST['tanggal_akhir'];
                                    if(empty($tanggal_awal) || empty($tanggal_akhir)){
                                    //jika data tanggal kosong
                                    ?>
                                    <script language="JavaScript">
                                        alert('Tanggal Awal dan Tanggal Akhir Harap di Isi!');
                                        document.location='laporan.php';
                                    </script>
                                    <?php
                                    }else{
                                    ?><i><b>Informasi : </b> Hasil pencarian data berdasarkan periode Tanggal <b><?php echo $_POST['tanggal_awal']?></b> s/d <b><?php echo $_POST['tanggal_akhir']?></b></i>
                                    
                                                <tr>
                                                    <th>No</th>
													<th>Kode Peminjaman</th>
													<th>Nama Barang</th>
													<th>Jumlah Pinjam</th>
                                                    <th>Tanggal Pinjam</th>
                                                    <th>Tanggal Kembali</th>
                                                    <th>Nama Pegawai</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
		include "koneksi.php";
		$no=1;	
		$select=mysqli_query($conn, "select * from peminjaman a left join detail_pinjam b on b.id_detail_pinjam=a.id_peminjaman
																left join pegawai c on c.id_pegawai=a.id_pegawai
																left join inventaris d on b.id_inventaris=d.id_inventaris
																where a.tanggal_pinjam BETWEEN '$_POST[tanggal_awal]' AND '$_POST[tanggal_akhir]'");
		while($data=mysqli_fetch_array($select))
		{
		?>
                                <tr class="success">
                                    <td><?php echo $no++; ?></td>
									<td><?php echo $data['kode_peminjaman'] ?></td>
									<td><?php echo $data['nama'] ?></td>
									<td><?php echo $data['jumlah_pinjam'] ?></td>
									<td><?php echo $data['tanggal_pinjam'] ?></td>
									<td><?php echo $data['tanggal_kembali'] ?></td>
									<td><?php echo $data['nama_pegawai'] ?></td>
                                </tr>
									<?php }  ?>
                                            
                                            </tbody>
                                        </table>
										<div class="pull-left">
										<a href="print_laporan.php?tanggal_awal=<?php echo $_POST['tanggal_awal']?>&tanggal_akhir=<?php echo $_POST['tanggal_akhir']?>" target="_blank" class="btn btn-primary">Print Data PDF</a>
						</div><br/><br/>
						<?php } } ?>
									<script type ="text/javascript" src="assets/js/jquery.min.js"></script>
									<script type ="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
									<script>$(document).ready(function(){
											$('#example').DataTable();
											});
									</script>
                                   </div>
                        <!-- /.col-lg-12 -->
                    </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->
		
		
        <!-- jQuery -->
        <script src="../js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>
		
        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>

        <!-- DataTables JavaScript -->
        <script src="../js/dataTables/jquery.dataTables.min.js"></script>
        <script src="../js/dataTables/dataTables.bootstrap.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="../js/startmin.js"></script>
		

        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
        <script>
            $(document).ready(function() {
                $('#dataTables-example').DataTable({
                        responsive: true
                });
            });
        </script>

    </body>
</html>