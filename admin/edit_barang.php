<?php
include('cek.php');
error_reporting(0);
?>
<?php

  $id_inventaris=$_GET['id_inventaris'];
include "koneksi.php"; ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Edit Barang | Admin</title>

        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- DataTables CSS -->
        <link href="../css/dataTables/dataTables.bootstrap.css" rel="stylesheet">

        <!-- DataTables Responsive CSS -->
        <link href="../css/dataTables/dataTables.responsive.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">
		<link href="css/datepicker.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header">
                    <a class="navbar-brand" href="inventaris.php"><font color="white">Inventaris Sarana dan Prasarana di SMK</font></a>
                </div>

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>


                <ul class="nav navbar-right navbar-top-links">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <font color="white"><i class="fa fa-user fa-fw"></i> <?php echo $_SESSION['nama_petugas'];?> <b class="caret"></b></font>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="profile.php"><i class="fa fa-user fa-fw"></i> Profile</a></li>
                            <li><a href="ubah_password.php"><i class="fa fa-key fa-fw"></i> Ubah Password</a></li>
                            <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
                <!-- /.navbar-top-links -->

               
                <?php
            
			if ($_SESSION['id_level']==1){
                echo'<div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form"><br/>
                                    <tr>
										<td colspan="2">&nbsp;&nbsp;<IMG width="200" height="200" SRC="images/user.png"></td>
									</tr>
                                </div><br/>
                                <!-- /input-group -->
                            </li>
							<li>
                                <a href="dashboard.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                            </li>
                            <li>
                                <a href="inventaris.php" class="active"><i class="fa fa-mail-forward fa-fw"></i> Inventarisir</a>
                            </li>
                            <li>
                                <a href="peminjaman.php"><i class="fa fa-edit fa-fw"></i> Peminjaman</a>
                            </li>
							<li>
                                <a href="pengembalian.php"><i class="fa fa-arrow-left fa-fw"></i> Pengembalian</a>
                            </li>
                           <li>
                                <a href="#"><i class="fa fa-gear fa-fw"></i> Lainnya<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="petugas.php">Petugas</a>
                                    </li>
									<li>
                                        <a href="laporan.php">Generate Laporan</a>
                                    </li>
									<li>
                                        <a href="logout.php">Logout</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                        </ul>
                    </div>
					<!---/penutup--->';
			}
			elseif ($_SESSION['id_level']==2){
				echo'<div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                     <tr>
										<td colspan="2"><IMG width="200" height="250" SRC="images/user.png"></td>
									</tr>
                                </div>
                                <!-- /input-group -->
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-edit fa-fw"></i> Peminjaman</a>
                            </li>
							<li>
                                <a href="#"><i class="fa fa-arrow-right fa-fw"></i> Pengembalian</a>
                            </li>
                           
                                </ul>
                                <!-- /.nav-second-level -->
                           
                </div><!---/penutup--->';
			}
			elseif ($_SESSION['id_level']==3){
				echo'<div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                     <tr>
										<td colspan="2"><IMG width="200" height="250" SRC="images/user.png"></td>
									</tr>
                                </div>
                                <!-- /input-group -->
                            </li>
							
                           
                            <li>
                                <a href="#"><i class="fa fa-edit fa-fw"></i> Peminjaman</a>
                            </li>
							<li>
                                <a href="#"><i class="fa fa-arrow-right fa-fw"></i> Logout</a>
                            </li>
                           
                                </ul>
                                <!-- /.nav-second-level -->
                           
                </div><!---/penutup--->';
			}
			?>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Barang</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
						<p align="right">
								<a href="barang.php" ><button type="button" class="btn btn-outline btn-danger fa fa-reply"> Kembali</button></a>
						</p>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                   Ubah Data Barang 
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-8">
										<?php
										 $id = $_GET['id_inventaris'];
									$select=mysqli_query($conn, "SELECT `id_inventaris`,`nama`,`kondisi`,`keterangan`,`jumlah`, a.`id_jenis`,`nama_jenis`,`tanggal_register`,a.`id_ruang`,`nama_ruang`,`kode_inventaris`,a.`id_petugas`,`nama_petugas` 
														FROM `inventaris` a
														left join jenis b on b.id_jenis=a.id_jenis
														left join ruang c on c.id_ruang=a.id_ruang
														left join petugas d on d.id_petugas=a.id_petugas
														WHERE id_inventaris='".$id."' "
			
														);

														$select_jenis  =  mysqli_query($conn, "SELECT `id_jenis`,`nama_jenis` FROM `jenis`");
														$select_ruang  =  mysqli_query($conn, "SELECT `id_ruang`,`nama_ruang` FROM `ruang`");
														$select_petugas=  mysqli_query($conn, "SELECT `id_petugas`,`nama_petugas` FROM `petugas`");
													$data=mysqli_fetch_array($select);
										?>
                                             <form action="update_barang.php?id_inventaris=<?php echo $id_inventaris?>" method="post" role="form" >
												<div class="form-group">
                                                    <label for="disabledSelect">Nomor</label>
                                                    <input value="<?php echo $data['id_inventaris'];?>" class="form-control" id="disabledInput" type="text" name="id_inventaris" disabled>
                                                 </div>
												<div class="form-group">
                                                    <label>Nama</label>
                                                    <input value="<?php echo $data['nama'];?>"  class="form-control" type="text" name="nama" autofocus required>
                                                </div>
												<div class="form-group">
                                                    <label>Kondisi</label>
                                                    <input value="<?php echo $data['kondisi'];?>"  class="form-control" type="text" name="kondisi" required>
                                                </div>
												 <div class="form-group">
                                                    <label>Keterangan</label>
                                                    <textarea  class="form-control" name="keterangan" rows="3" required><?php echo $data['keterangan'];?></textarea>
                                                </div>
												<div class="form-group">
                                                    <label>Jumlah</label>
                                                    <input value="<?php echo $data['jumlah'];?>"  class="form-control" type="number" name="jumlah" required>
                                                </div>
												<div class="form-group">
												<label>Jenis Barang</label>
													  <div class="controls">
														<select class="form-control span8" name="id_jenis">
												<?php while($data_jenis=mysqli_fetch_array($select_jenis)){ ?>
														  <option value="<?php echo $data_jenis['id_jenis']; ?>"
															<?php if($data['id_jenis']==$data_jenis['id_jenis']){ echo "selected"; }?>>
															<?php echo $data_jenis['nama_jenis']; ?>
														  </option>
												<?php } ?>
														</select>
													  </div>
												</div>
												
												<div class="form-group">			
												<label>Tanggal Register</label>		
											<div class="input-group date " data-date="" data-date-format="yyyy-mm-dd">
													<input value="<?php echo $data['tanggal_register'];?>" name="tanggal_register" class="form-control" type="text" readonly="readonly">
													<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
											</div>
												</div>
												
												<div class="form-group">
												<label>Ruang</label>
													  <div class="controls">
														<select class="form-control span8" name="id_ruang">
												<?php while($data_ruang=mysqli_fetch_array($select_ruang)){ ?>
														  <option value="<?php echo $data_ruang['id_ruang']; ?>"
															<?php if($data['id_ruang']==$data_ruang['id_ruang']){ echo "selected"; }?>>
															<?php echo $data_ruang['nama_ruang']; ?>
														  </option>
												<?php } ?>
														</select>
													  </div>
												</div>
												<div class="form-group">
                                                    <label>Kode Inventaris</label>
                                                    <input value="<?php echo $data['kode_inventaris'];?>"  class="form-control" type="text" name="kode_inventaris" readonly>
                                                </div>
												<div class="form-group">
												<label>Nama Petugas</label>
													  <div class="controls">
														<select class="form-control span8" name="id_petugas">
												<?php while($data_petugas=mysqli_fetch_array($select_petugas)){ ?>
														  <option value="<?php echo $data_petugas['id_petugas']; ?>"
															<?php if($data['id_petugas']==$data_petugas['id_petugas']){ echo "selected"; }?>>
															<?php echo $data_petugas['nama_petugas']; ?>
														  </option>
												<?php } ?>
														</select>
													  </div>
												</div>
													<div class="form-group">
														<input class="btn btn-primary" type="submit" name="submit" value="Ubah" />
														<input class="btn btn-danger" type="reset" name="reset" value="Reset" />
													</div>
                                            </form>
                                        </div>
                                        <!-- /.col-lg-6 (nested) -->
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                    <!-- /.table-responsive -->
                                   </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="../js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>

        <!-- DataTables JavaScript -->
        <script src="../js/dataTables/jquery.dataTables.min.js"></script>
        <script src="../js/dataTables/dataTables.bootstrap.min.js"></script>

		<!-- daterangepicker -->
    <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>

    <script src="js/custom.js"></script>
    <script>
    $(".input-group.date").datepicker({endDate:"0d"});
    </script>
	
    <!-- /datepicker -->
		
        <!-- Custom Theme JavaScript -->
        <script src="../js/startmin.js"></script>

        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
        <script>
            $(document).ready(function() {
                $('#dataTables-example').DataTable({
                        responsive: true
                });
            });
        </script>
    </body>
</html>
