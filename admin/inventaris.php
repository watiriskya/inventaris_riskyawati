<?php
include('cek.php');
error_reporting(0);
?>
<?php
include('cek_level.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Inventaris | Admin</title>

        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- DataTables CSS -->
        <link href="../css/dataTables/dataTables.bootstrap.css" rel="stylesheet">

        <!-- DataTables Responsive CSS -->
        <link href="../css/dataTables/dataTables.responsive.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">
		
		<link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#"><font color="white">Inventaris Sarana dan Prasarana di SMK</font></a>
                </div>

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>


                <ul class="nav navbar-right navbar-top-links">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <font color="white"><i class="fa fa-user fa-fw"></i> <?php echo $_SESSION['nama_petugas'];?>  <b class="caret"></b></font>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="profile.php"><i class="fa fa-user fa-fw"></i> Profile</a></li>
                            <li><a href="ubah_password.php"><i class="fa fa-key fa-fw"></i> Ubah Password</a></li>
                            <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
                <!-- /.navbar-top-links -->
          <?php
            
			if ($_SESSION['id_level']==1){
                echo'<div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form"><br/>
                                     <tr>
										<td colspan="3">&nbsp;&nbsp;<IMG width="200" height="200" SRC="images/user.png"></td>
									</tr>
                                </div><br/>
                                <!-- /input-group -->
                            </li>
							<li>
                                <a href="dashboard.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                            </li>
                            <li>
                                <a href="inventaris.php"><i class="fa fa-mail-forward fa-fw"></i> Inventarisir</a>
                            </li>
							<li>
                                <a href="peminjaman.php"><i class="fa fa-edit fa-fw"></i> Peminjaman</a>
                            </li>
							<li>
                                <a href="pengembalian.php"><i class="fa fa-arrow-left fa-fw"></i> Pengembalian</a>
                            </li>
                           <li>
                                <a href="#"><i class="fa fa-gear fa-fw"></i> Lainnya<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="petugas.php">Petugas</a>
                                    </li>
									<li>
                                        <a href="laporan.php">Generate Laporan</a>
                                    </li>
									<li>
                                        <a href="logout.php">Logout</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Inventaris</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                 <div class="row">
				   <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-cube fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div><h4>Barang</h4></div>
                                </div>
                            </div>
                        </div>
                        <a href="barang.php">
                            <div class="panel-footer">
                                <span class="pull-left">Selengkapnya</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
				<div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div><h4>Jenis</h4></div>
                                </div>
                            </div>
                        </div>
                        <a href="jenis.php">
                            <div class="panel-footer">
                                <span class="pull-left">Selengkapnya</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
				<div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-home fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div><h4>Ruang</h4></div>
                                </div>
                            </div>
                        </div>
                        <a href="ruang.php">
                            <div class="panel-footer">
                                <span class="pull-left">Selengkapnya</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
				<div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-users fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div><h4>Pegawai</h4></div>
                                </div>
                            </div>
                        </div>
                        <a href="pegawai.php">
                            <div class="panel-footer">
                                <span class="pull-left">Selengkapnya</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div><!---/penutup--->';
			}
			?>
						
				</div>				
            <!-- /.row -->
			<div class="row">
			<div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Level
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Level</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
									include "koneksi.php";
									$no=1;
									$select=mysqli_query($conn, "SELECT * FROM level");
									while($data=mysqli_fetch_array($select))
									{
								?>
                        
                                <tr class="">
                                    <td><?php echo $no++; ?></td>
									<td><?php echo $data['nama_level'] ?></td>
									<td>
										<a href="#"><button type="button" class="btn btn-primary fa fa-edit" data-toggle="modal" data-target="#myModal<?php echo $data['id_level']; ?>"> </button></a>
										<a href="hapus_level.php?id_level=<?php echo $data['id_level']; ?>"><button type="button" class="btn btn-danger fa fa-trash"> </button></a>
									</td>
                                </tr>
								
			<div class="modal fade" id="myModal<?php echo $data['id_level']; ?>" role="dialog">
              <div class="modal-dialog">
              
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update Data Level</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form" action="update_level.php" method="get">

                        <?php
                        $id_level = $data['id_level']; 
                        $query_edit = mysqli_query($conn, "SELECT * FROM level WHERE id_level='$id_level'");
                        //$result = mysqli_query($conn, $query);
                        while ($row = mysqli_fetch_array($query_edit)) {  
                        ?>

                        <input type="hidden" name="id_level" value="<?php echo $row['id_level']; ?>">

                        <div class="form-group">
                          <label>Nama</label>
                          <input type="text" name="nama_level" class="form-control" value="<?php echo $row['nama_level']; ?>">      
                        </div>
                        
                        <div class="modal-footer">  
                          <button type="submit" class="btn btn-success">Update</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>

                        <?php 
                        }
                        //mysql_close($host);
                        ?>        
                      </form>
                  </div>
                </div>
                
              </div>
            </div>
								
								<?php } ?>
								
								      </tbody>
                                </table>
								<td>
									<a href="#"><button type="button" class="btn btn-outline btn-primary fa fa-plus" data-toggle="modal" data-target="#myModal"> Tambah Level</button></a>
								</td>
								<div class="modal fade" id="myModal" role="dialog">
              <div class="modal-dialog">
              
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Data Level</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form" action="simpan_level.php" method="post">

                        <div class="form-group">
                          <label>Nama Level</label>
                          <input type="text" name="nama_level" class="form-control" placeholder="Masukkan Nama Level Baru" required autofocus>      
                        </div>
                        
                        <div class="modal-footer">  
                          <button type="submit" class="btn btn-success">Simpan</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
    
                      </form>
                  </div>
                </div>
                
              </div>
            </div>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
			
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->
		
		
        <!-- jQuery -->
        <script src="../js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>
		
        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>

        <!-- DataTables JavaScript -->
        <script src="../js/dataTables/jquery.dataTables.min.js"></script>
        <script src="../js/dataTables/dataTables.bootstrap.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="../js/startmin.js"></script>
		

        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
        <script>
            $(document).ready(function() {
                $('#dataTables-example').DataTable({
                        responsive: true
                });
            });
        </script>

    </body>
</html>