<?php
include('cek.php');
error_reporting(0);
?>
<?php
include "koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Pengembalian Barang | Admin</title>

        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- DataTables CSS -->
        <link href="../css/dataTables/dataTables.bootstrap.css" rel="stylesheet">

        <!-- DataTables Responsive CSS -->
        <link href="../css/dataTables/dataTables.responsive.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header">
                    <a class="navbar-brand" href="inventaris.php"><font color="white">Inventaris Sarana dan Prasarana di SMK</font></a>
                </div>

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>


                <ul class="nav navbar-right navbar-top-links">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <font color="white"><i class="fa fa-user fa-fw"></i> <?php echo $_SESSION['nama_petugas'];?> <b class="caret"></b></font>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="profile.php"><i class="fa fa-user fa-fw"></i> Profile</a></li>
                            <li><a href="ubah_password.php"><i class="fa fa-key fa-fw"></i> Ubah Password</a></li>
                            <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
                <!-- /.navbar-top-links -->

                <?php
            
			if ($_SESSION['id_level']==1){
                echo'<div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form"><br/>
                                    <tr>
										<td colspan="2">&nbsp;&nbsp;<IMG width="200" height="200" SRC="images/user.png"></td>
									</tr>
                                </div><br/>
                                <!-- /input-group -->
                            </li>
							<li>
                                <a href="dashboard.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                            </li>
                            <li>
                                <a href="inventaris.php"><i class="fa fa-mail-forward fa-fw"></i> Inventarisir</a>
                            </li>
                            <li>
                                <a href="peminjaman.php"><i class="fa fa-edit fa-fw"></i> Peminjaman</a>
                            </li>
							<li>
                                <a href="pengembalian.php" class="active"><i class="fa fa-arrow-left fa-fw"></i> Pengembalian</a>
                            </li>
                           <li>
                                <a href="#"><i class="fa fa-gear fa-fw"></i> Lainnya<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="petugas.php">Petugas</a>
                                    </li>
									<li>
                                        <a href="laporan.php">Generate Laporan</a>
                                    </li>
									<li>
                                        <a href="logout.php">Logout</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Pengembalian</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                 <div class="row">
				
                </div><!---/penutup--->';
			}
			elseif ($_SESSION['id_level']==2){
				echo'<div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                     <tr>
										<td colspan="2"><IMG width="200" height="250" SRC="images/user.png"></td>
									</tr>
                                </div>
                                <!-- /input-group -->
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-edit fa-fw"></i> Peminjaman</a>
                            </li>
							<li>
                                <a href="#"><i class="fa fa-arrow-right fa-fw"></i> Pengembalian</a>
                            </li>
                           
                                </ul>
                                <!-- /.nav-second-level -->
                           
                </div><!---/penutup--->';
			}
			elseif ($_SESSION['id_level']==3){
				echo'<div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                     <tr>
										<td colspan="2"><IMG width="200" height="250" SRC="images/user.png"></td>
									</tr>
                                </div>
                                <!-- /input-group -->
                            </li>
							
                           
                            <li>
                                <a href="#"><i class="fa fa-edit fa-fw"></i> Peminjaman</a>
                            </li>
							<li>
                                <a href="#"><i class="fa fa-arrow-right fa-fw"></i> Logout</a>
                            </li>
                           
                                </ul>
                                <!-- /.nav-second-level -->
                           
                </div><!---/penutup--->';
			}
			?>
						
				</div>	
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
						<div class="pull-right">
								<a href="inventaris.php"><button type="button" class="btn btn-outline btn-danger fa fa-reply"> Back To Inventarisir </button></a>
						</div><br/><br/>
							<div class="panel panel-default">
                                <div class="panel-heading">
                                    Pengembalian 
                                </div>
								<div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <form method="post">
											   <select name="id_peminjaman" class="form-control m-bot15">
													<?php
													include "koneksi.php";
															//display values in combobox/dropdown
													$result = mysqli_query($conn, "SELECT id_peminjaman,kode_peminjaman from peminjaman where status_peminjaman='Dipinjam' ");
													while($row = mysqli_fetch_assoc($result))
													{
														echo "<option value='$row[id_peminjaman]'>$row[kode_peminjaman]</option>";
													} 
													?>
											   </select><br/>
													<div class="form-group">
														<input class="btn btn-primary" type="submit" name="pilih" value="Tampilkan" />
													</div>
                                            </form>
                                        </div>
                                        <!-- /.col-lg-6 (nested) -->
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
								<?php if(isset($_POST['pilih'])){?>
								<form action="proses_pengembalian.php" method="post" enctype="multipart/form-data">
								<?php
									include "koneksi.php";
									$id_peminjaman=$_POST['id_peminjaman'];
									$select=mysqli_query($conn,"select * from peminjaman a left join detail_pinjam b on b.id_detail_pinjam=a.id_peminjaman
																left join inventaris c on b.id_inventaris=c.id_inventaris
																left join pegawai d on a.id_pegawai=d.id_pegawai
																where id_peminjaman='$id_peminjaman'");
									while($data=mysqli_fetch_array($select))  {
								?>
                        
                                <div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">
										Id Inventaris
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input name="id_peminjaman" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $data['id_peminjaman'];?>" autocomplete="off" maxlength="11" required="">
										<input name="id_detail_pinjam" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $data['id_detail_pinjam'];?>" autocomplete="off" maxlength="11" required="">
										<input name="id_inventaris" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['id_inventaris'];?>.<?php echo $data['nama'];?>" autocomplete="off" maxlength="11" required="" readonly>
									</div>
								</div><br><br><br>    
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" >
										Tanggal Pinjam
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="datetime" name="" value="<?php echo $data['tanggal_pinjam']?>" class="form-control col-md-7 col-xs-12" disabled placeholder="tanggal_pinjam">
										<input type="hidden" name="tanggal_pinjam" value="<?php echo $data['tanggal_pinjam']?>" >
										<input name="tanggal_kembali" type="hidden" required>
									</div>
								</div><br><br>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" >
										ID Pegawai
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="text" name="" value="<?php echo $data['id_pegawai']?>.<?php echo $data['nama_pegawai'];?>" class="form-control col-md-7 col-xs-12" disabled placeholder="ID Pegawai">
										<input type="hidden" name="id_pegawai" value="<?php echo $data['id_pegawai']?>" >
									</div>
								</div><br><br>
							  <div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" >
									Jumlah Pinjam
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input type="number" name="jumlah_pinjam" value="<?php echo $data['jumlah_pinjam']?>" class="form-control col-md-7 col-xs-12"  placeholder="Jumlah" readonly>
								</div>
							</div><br><br>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" >
								 Status Peminjaman
							 </label>
							 <div class="col-md-6 col-sm-6 col-xs-12">
							  <select name="status_peminjaman" class="form-control m-bot15" readonly>
								<option>Dikembalikan</option>
							</select>
						</div><br><br><br>
						<div class="form-group">
							&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary" /> Simpan</button>
						</div>
					</div>
				<?php } ?>
			</form>
			<?php } ?>
								<div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="example">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Tanggal Pinjam</th>
                                                    <th>Tanggal Kembali</th>
                                                    <th>Status Peminjaman</th>
                                                    <th>Nama Pegawai</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <?php
								include "koneksi.php";
								$no=1;
								$select=mysqli_query($conn,"select * from peminjaman a left join pegawai c on c.id_pegawai=a.id_pegawai where status_peminjaman='Dikembalikan'");
								while($data=mysqli_fetch_array($select))
										{
								?>
                        
                                <tr class="success">
									<td><?php echo $no++; ?></td>
                                    <td><?php echo $data['tanggal_pinjam'] ?></td>
									<td><?php echo $data['tanggal_kembali'] ?></td>
									<td><?php echo $data['status_peminjaman'] ?></td>
									<td><?php echo $data['nama_pegawai'] ?></td>
                                </tr>
								<?php } ?>
                                            
                                            </tbody>
                                        </table>
									<script type ="text/javascript" src="assets/js/jquery.min.js"></script>
									<script type ="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
									<script>$(document).ready(function(){
											$('#example').DataTable();
											});
									</script>
                                    </div>
                                    <!-- /.table-responsive -->
                                   </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->
		
		<!-- jQuery -->
        <script src="../js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>

        <!-- DataTables JavaScript -->
        <script src="../js/dataTables/jquery.dataTables.min.js"></script>
        <script src="../js/dataTables/dataTables.bootstrap.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="../js/startmin.js"></script>

        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
        <script>
            $(document).ready(function() {
                $('#dataTables-example').DataTable({
                        responsive: true
                });
            });
        </script>

    </body>
</html>
