<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "ujikom_ku";
 
    // membuat koneksi
    $koneksi = new mysqli($servername, $username, $password, $dbname);
 
    // melakukan pengecekan koneksi
    if ($koneksi->connect_error) {
        die("Connection failed: " . $koneksi->connect_error);
    } 
 
    if($_POST['rowid']) {
        $id_inventaris = $_POST['rowid'];
        // mengambil data berdasarkan id
        $sql = "SELECT * FROM inventaris a left join jenis b on b.id_jenis=a.id_jenis 
									  left join ruang c on c.id_ruang=a.id_ruang
									  left join petugas d on d.id_petugas=a.id_petugas WHERE id_inventaris = $id_inventaris";
        $result = $koneksi->query($sql);
        foreach ($result as $baris) { 
					$date = $baris['tanggal_register'];
					$tanggal = date ('d F Y', Strtotime ($date));
		?>
            <table class="table">
				<tr>
                    <td>Kode Inventaris</td>
                    <td>:</td>
                    <td><?php echo $baris['kode_inventaris']; ?></td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td><?php echo $baris['nama']; ?></td>
                </tr>
                <tr>
                    <td>Kondisi</td>
                    <td>:</td>
                    <td><?php echo $baris['kondisi']; ?></td>
                </tr>
				<tr>
                    <td>Keterangan</td>
                    <td>:</td>
                    <td><?php echo $baris['keterangan']; ?></td>
                </tr>
				<tr>
                    <td>Jumlah</td>
                    <td>:</td>
                    <td><?php echo $baris['jumlah']; ?></td>
                </tr>
				<tr>
                    <td>ID Jenis</td>
                    <td>:</td>
                    <td><?php echo $baris['kode_jenis']; ?> - <?php echo $baris['nama_jenis']; ?></td>
                </tr>
                <tr>
                    <td>Tanggal Register</td>
                    <td>:</td>
                    <td><?php echo $tanggal; ?></td>
                </tr>
				<tr>
                    <td>ID Ruang</td>
                    <td>:</td>
                    <td><?php echo $baris['kode_ruang']; ?> - <?php echo $baris['nama_ruang']; ?></td>
                </tr>
				<tr>
                    <td>Nama Petugas</td>
                    <td>:</td>
                    <td><?php echo $baris['nama_petugas']; ?></td>
                </tr>
            </table>
        <?php 
 
        }
    }
    $koneksi->close();
?>