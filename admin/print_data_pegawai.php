<?php ob_start(); ?>
<html>
<head>
	<title>Cetak PDF</title>

	<style>
	table {border-collapse:collapse; table-layout:fixed;width: 630px:}
	table td {word-wrap:break-word;width: 20%}
	</style>
</head>
<body>
<h1 style="text-align:center;">Data Pegawai</h1><br/>
<table align="center" border="1" width="100%">
<tr>
	<th align="center"> No </th>
	<th align="center">NIP</th>
	<th align="center">Nama Pegawai</th>
	<th align="center">Alamat</th>
</tr>
		<?php
		include "koneksi.php";
		$no=1;
		$select=mysqli_query($conn, "SELECT * FROM pegawai");
		while($data=mysqli_fetch_array($select))
		{
		?>
		<tr>
			<td width="15%" align="center"><?php echo $no++; ?></td>
			<td align="center"><?php echo $data['nip']; ?></td>
			<td align="center"><?php echo $data['nama_pegawai']; ?></td>
			<td align="center"><?php echo $data['alamat']; ?></td>
		</tr>
		<?php
		}
		?>
</table>
</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();

require_once('html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('P','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data Pegawai.pdf', 'D');
?>