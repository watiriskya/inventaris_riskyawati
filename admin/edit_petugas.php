<?php
include('cek.php');
error_reporting(0);
?>
<?php

  $id_petugas=$_GET['id_petugas'];
include "koneksi.php"; ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Edit Petugas | Admin</title>

        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="../css/metisMenu.min.css" rel="stylesheet">

        <!-- DataTables CSS -->
        <link href="../css/dataTables/dataTables.bootstrap.css" rel="stylesheet">

        <!-- DataTables Responsive CSS -->
        <link href="../css/dataTables/dataTables.responsive.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/startmin.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header">
                    <a class="navbar-brand" href="inventaris.php"><font color="white">Inventaris Sarana dan Prasarana di SMK</font></a>
                </div>

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>


                <ul class="nav navbar-right navbar-top-links">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <font color="white"><i class="fa fa-user fa-fw"></i> <?php echo $_SESSION['nama_petugas'];?> <b class="caret"></b></font>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="profile.php"><i class="fa fa-user fa-fw"></i> Profile</a></li>
                            <li><a href="ubah_password.php"><i class="fa fa-key fa-fw"></i> Ubah Password</a></li>
                            <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
                <!-- /.navbar-top-links -->

                <?php
            
			if ($_SESSION['id_level']==1){
                echo'<div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form"><br/>
                                    <tr>
										<td colspan="2">&nbsp;&nbsp;<IMG width="200" height="200" SRC="images/user.png"></td>
									</tr>
                                </div><br/>
                                <!-- /input-group -->
                            </li>
							<li>
                                <a href="dashboard.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                            </li>
                            <li>
                                <a href="inventaris.php"><i class="fa fa-mail-forward fa-fw"></i> Inventarisir</a>
                            </li>
                            <li>
                                <a href="peminjaman.php"><i class="fa fa-edit fa-fw"></i> Peminjaman</a>
                            </li>
							<li>
                                <a href="pengembalian.php"><i class="fa fa-arrow-left fa-fw"></i> Pengembalian</a>
                            </li>
                           <li>
                                <a href="#"><i class="fa fa-gear fa-fw"></i> Lainnya<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="petugas.php">Petugas</a>
                                    </li>
									<li>
                                        <a href="laporan.php">Generate Laporan</a>
                                    </li>
									<li>
                                        <a href="logout.php">Logout</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
					<!---/penutup--->';
			}
			elseif ($_SESSION['id_level']==2){
				echo'<div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                     <tr>
										<td colspan="2"><IMG width="200" height="250" SRC="images/user.png"></td>
									</tr>
                                </div>
                                <!-- /input-group -->
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-edit fa-fw"></i> Peminjaman</a>
                            </li>
							<li>
                                <a href="#"><i class="fa fa-arrow-right fa-fw"></i> Pengembalian</a>
                            </li>
                           
                                </ul>
                                <!-- /.nav-second-level -->
                           
                </div><!---/penutup--->';
			}
			elseif ($_SESSION['id_level']==3){
				echo'<div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                     <tr>
										<td colspan="2"><IMG width="200" height="250" SRC="images/user.png"></td>
									</tr>
                                </div>
                                <!-- /input-group -->
                            </li>
							
                           
                            <li>
                                <a href="#"><i class="fa fa-edit fa-fw"></i> Peminjaman</a>
                            </li>
							<li>
                                <a href="#"><i class="fa fa-arrow-right fa-fw"></i> Logout</a>
                            </li>
                           
                                </ul>
                                <!-- /.nav-second-level -->
                           
                </div><!---/penutup--->';
			}
			?>
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Petugas</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
						<p align="right">
								<a href="petugas.php" ><button type="button" class="btn btn-outline btn-danger fa fa-reply"> Kembali</button></a>
						</p>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                   Ubah Data Petugas 
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-8">
										<?php
										 $id = $_GET['id_petugas'];
									$select=mysqli_query($conn, "SELECT `id_petugas`,`username`,`password`,`nama_petugas`, a.`id_level`,`nama_level` 
														FROM `petugas` a
														left join level b on b.id_level=a.id_level
														WHERE id_petugas='".$id."' "
			
														);

														$select_level   =  mysqli_query($conn, "SELECT `id_level`,`nama_level` FROM `level`");
													$data=mysqli_fetch_array($select);
										?>
                                            <form action="update_petugas.php?id_petugas=<?php echo $id_petugas?>" method="post" role="form" >
												<div class="form-group">
                                                        <label for="disabledSelect">Nomor</label>
                                                        <input value="<?php echo $data['id_petugas'];?>" class="form-control" id="disabledInput" type="text" name="id_petugas" disabled>
                                                 </div>
												<div class="form-group">
                                                    <label>Username</label>
                                                    <input value="<?php echo $data['username'];?>"  class="form-control" name="username" autofocus required>
                                                </div>
												<div class="form-group">
                                                    <label>Password</label>
                                                    <input value="<?php echo $data['password'];?>"  class="form-control" type="text" name="password" required>
                                                </div>
												<div class="form-group">
                                                    <label>Nama Petugas</label>
                                                    <input value="<?php echo $data['nama_petugas'];?>"  class="form-control" name="nama_petugas" readonly>
                                                </div>
												<div class="form-group">
												<label>Level</label>
												
												 <div class="controls">
															<select class="form-control span8" name="id_level">
													<?php while($data_level=mysqli_fetch_array($select_level)){ ?>
															  <option value="<?php echo $data_level['id_level']; ?>"
																<?php if($data['id_level']==$data_level['id_level']){ echo "selected"; }?>>
																<?php echo $data_level['nama_level']; ?>
															  </option>
													<?php } ?>
															</select>
														  </div>
												</div>
													<div class="form-group">
														<input class="btn btn-primary" type="submit" name="submit" value="Ubah" />
														<input class="btn btn-danger" type="reset" name="reset" value="Reset" />
													</div>
                                            </form>
                                        </div>
                                        <!-- /.col-lg-6 (nested) -->
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                    <!-- /.table-responsive -->
                                   </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="../js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../js/metisMenu.min.js"></script>

        <!-- DataTables JavaScript -->
        <script src="../js/dataTables/jquery.dataTables.min.js"></script>
        <script src="../js/dataTables/dataTables.bootstrap.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="../js/startmin.js"></script>

        <!-- Page-Level Demo Scripts - Tables - Use for reference -->
        <script>
            $(document).ready(function() {
                $('#dataTables-example').DataTable({
                        responsive: true
                });
            });
        </script>
<script type="text/javascript">
$(document).ready(function(){
	$('.form-checkbox').click(function(){
		if($(this).is(':checked')){
			$('.form-password').attr('type','text');
		} else {
			$('.form-password').attr('type','password');
		}
	});
});
</script>
    </body>
</html>
