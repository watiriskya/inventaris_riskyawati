<?php ob_start(); ?>
<html>
<head>
	<title>Cetak PDF</title>

	<style>
	table {border-collapse:collapse; table-layout:fixed;width: 630px:}
	table td {word-wrap:break-word;width: 16%}
	</style>
</head>
<body>
<h1 style="text-align:center;">Laporan Pertahun</h1><br/>
<table align="center" border="1" width="100%">
<tr>
	<th align="center">No</th>
	<th align="center">Kode Peminjaman</th>
	<th align="center">Nama Barang</th>
	<th align="center">Jumlah Pinjam</th>
	<th align="center">Tanggal Pinjam</th>
	<th align="center">Tanggal Kembali</th>
	<th align="center">Nama Pegawai</th>
</tr>
		<?php
		include "koneksi.php";
		$no=1;
		if(isset($_POST['tahun'])){ 
		$select=mysqli_query($conn,"select * from peminjaman left join pegawai on peminjaman.id_pegawai=pegawai.id_pegawai
															 left join detail_pinjam on detail_pinjam.id_detail_pinjam=peminjaman.id_peminjaman
															 left join inventaris on inventaris.id_inventaris=detail_pinjam.id_inventaris WHERE YEAR(tanggal_pinjam) = '$_POST[tahun]'");
		while($data=mysqli_fetch_array($select))
										{
		?>
		<tr>
			<td width="10%"><?php echo $no++; ?></td>
			<td align="center"><?php echo $data['kode_peminjaman']; ?></td>
			<td align="center"><?php echo $data['nama']; ?></td>
			<td align="center"><?php echo $data['jumlah_pinjam']; ?></td>
			<td align="center"><?php echo $data['tanggal_pinjam']; ?></td>
			<td align="center"><?php echo $data['tanggal_kembali']; ?></td>
			<td align="center"><?php echo $data['nama_pegawai']; ?></td>
		</tr>
		<?php 
		} 
		 } 
		 ?>
</table>
</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();

require_once('html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('L','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Laporan Pertahun.pdf', 'D');
?>