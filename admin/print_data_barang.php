<?php ob_start(); ?>
<html>
<head>
	<title>Cetak PDF</title>

	<style>
	table {border-collapse:collapse; table-layout:fixed;width: 630px:}
	table td {word-wrap:break-word;width: 10.5%}
	</style>
</head>
<body>
<h1 style="text-align:center;">Data Barang</h1><br/>
<table align="center" border="1" width="100%">
<tr>
	<th align="center"> No </th>
	<th align="center">Nama Barang</th>
	<th align="center">Kondisi Barang</th>
	<th align="center">Keterangan</th>
	<th align="center">Jumlah Barang</th>
	<th align="center">Jenis</th>
	<th align="center">Tanggal Register</th>
	<th align="center">Ruang</th>
	<th align="center">Kode Inventaris</th>
	<th align="center">Nama Petugas</th>
</tr>
		<?php
		include "koneksi.php";
		$no=1;
		$select=mysqli_query($conn, "SELECT * FROM inventaris a left join jenis b on b.id_jenis=a.id_jenis 
									  left join ruang c on c.id_ruang=a.id_ruang
									  left join petugas d on d.id_petugas=a.id_petugas");
		while($data=mysqli_fetch_array($select))
		{
		?>
		<tr>
			<td width="15%" align="center"><?php echo $no++; ?></td>
			<td align="center"><?php echo $data['nama']; ?></td>
			<td align="center"><?php echo $data['kondisi']; ?></td>
			<td align="center"><?php echo $data['keterangan']; ?></td>
			<td align="center"><?php echo $data['jumlah']; ?></td>
			<td align="center"><?php echo $data['nama_jenis']; ?></td>
			<td align="center"><?php echo $data['tanggal_register']; ?></td>
			<td align="center"><?php echo $data['nama_ruang']; ?></td>
			<td align="center"><?php echo $data['kode_inventaris']; ?></td>
			<td align="center"><?php echo $data['nama_petugas']; ?></td>
		</tr>
		<?php
		}
		?>
</table>
</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();

require_once('html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('L','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data Barang.pdf', 'D');
?>