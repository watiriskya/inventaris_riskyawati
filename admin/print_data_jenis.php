<?php ob_start(); ?>
<html>
<head>
	<title>Cetak PDF</title>

	<style>
	table {border-collapse:collapse; table-layout:fixed;width: 630px:}
	table td {word-wrap:break-word;width: 20%}
	</style>
</head>
<body>
<h1 style="text-align:center;">Data Jenis</h1><br/>
<table align="center" border="1" width="100%">
<tr>
	<th align="center">No</th>
	<th align="center">Kode Jenis</th>
	<th align="center">Nama Jenis</th>
	<th align="center">Keterangan</th>
</tr>
		<?php
		include "koneksi.php";
		$no=1;
		$select=mysqli_query($conn, "SELECT * FROM jenis");
		while($data=mysqli_fetch_array($select))
		{
		?>
		<tr>
			<td width="15%" align="center"><?php echo $no++; ?></td>
			<td align="center"><?php echo $data['kode_jenis']; ?></td>
			<td align="center"><?php echo $data['nama_jenis']; ?></td>
			<td align="center"><?php echo $data['keterangan_jenis']; ?></td>
		</tr>
		<?php
		}
		?>
</table>
</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();

require_once('html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('P','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data Jenis.pdf', 'D');
?>