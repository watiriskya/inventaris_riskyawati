<?php
session_start();
include "koneksi.php";
?>
<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>Aplikasi Sarana dan Prasarana di SMK</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/premium.css">
		<!-- Link Buat Header -->
		<link href="admin/../css/bootstrap.min.css" rel="stylesheet">
        <link href="admin/../css/metisMenu.min.css" rel="stylesheet">
        <link href="admin/../css/dataTables/dataTables.bootstrap.css" rel="stylesheet">
        <link href="admin/../css/dataTables/dataTables.responsive.css" rel="stylesheet">
        <link href="admin/../css/startmin.css" rel="stylesheet">
        <link href="admin/../css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
	<body>
		<div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <ul class="nav navbar-right navbar-top-links">
                     <li><a href="index.php"><font color="white"><i class="fa fa-sign-out fa-fw"></i> Back To Dashboard</font></a></li>
                </ul>
                <!-- /.navbar-top-links -->
		</div>	<br/><br/><br/>			
<div class="dialog">
    <div class="panel panel-default">
        <p class="panel-heading no-collapse">Login</p>
        <div class="panel-body">
            <form action="cek_login.php" method="post">
                <div class="form-group">
                    <label>Username</label>
                    <input type="text" name="username" class="form-control span12" autocomplete="off" autofocus required>
                </div>
                <div class="form-group">
                <label>Password</label>
                    <input type="password"  name="password" class="form-control span12" required>
                </div>
                <input type="submit" name="submit" value="Masuk" class="btn btn-primary pull-right"></a>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>
</div>
    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
</body></html>
