<?php
include('cek.php');
error_reporting(0);
?>
<?php
include('cek_level.php');
?>
<?php
include "koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Detail Pinjam | Peminjam</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/3-col-portfolio.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../assets/css/jquery.dataTables.css">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="peminjaman.php">Inventaris Sarana dan Prasarana di SMK</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="peminjaman.php">Peminjaman
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="logout.php">Logout</a>
            </li>
            <li class="nav-item">
              <a class="nav-link"><font color="white"><i class="fa fa-user fa-fw"></i> <?php echo $_SESSION['nama_petugas'];?></font></a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">
      <!-- Page Heading -->
      <h1 class="my-4"><p align="center">Detail Pinjam</p></h1><br/><br/>
  <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
                            
                                <!-- /.panel-heading -->
	                            <div class="panel-body">
								<?php
								include "koneksi.php";
								$id_peminjaman=$_GET['id_peminjaman'];
								$select=mysqli_query($conn, "select * from peminjaman left join pegawai on peminjaman.id_pegawai=pegawai.id_pegawai
																				left join detail_pinjam on peminjaman.id_peminjaman=detail_pinjam.id_detail_pinjam
																				left join inventaris on detail_pinjam.id_inventaris=inventaris.id_inventaris
																				where id_peminjaman='$id_peminjaman'");
																				while($row=mysqli_fetch_array($select)){
								?>
                                        <table class="table">
                <tr>
                    <td>Kode Peminjaman</td>
                    <td>:</td>
                    <td><?php echo $row['kode_peminjaman']; ?></td>
                </tr>
				<tr>
                    <td>Nama Barang</td>
                    <td>:</td>
                    <td><?php echo $row['nama']; ?></td>
                </tr>
                <tr>
                    <td>Jumlah Pinjam</td>
                    <td>:</td>
                    <td><?php echo $row['jumlah_pinjam']; ?></td>
                </tr>
				<tr>
                    <td>Tanggal Pinjam</td>
                    <td>:</td>
                    <td><?php echo $row['tanggal_pinjam']; ?></td>
                </tr>
				<tr>
                    <td>Status Peminjaman</td>
                    <td>:</td>
                    <td><?php echo $row['status_peminjaman']; ?></td>
                </tr>
				<tr>
                    <td>Nama Pegawai</td>
                    <td>:</td>
                    <td><?php echo $row['nama_pegawai']; ?></td>
                </tr>
																				<?php } ?>
            </table>
                                   </div>
                                <!-- /.panel-body --><br/>
								<p align="left">
								<a href="peminjaman.php" ><button type="button" class="btn btn-outline btn-primary fa fa-reply"> Kembali</button></a>
							</p>   
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                </div>
    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Inventaris Sarana dan Prasarana di SMK 2019</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
