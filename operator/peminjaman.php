<?php
include('cek.php');
error_reporting(0);
?>
<?php
include('cek_level.php');
?>
<?php
include "koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Peminjaman | Operator</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/3-col-portfolio.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../assets/css/jquery.dataTables.css">

  </head>

  <body>

    <!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="peminjaman.php">Inventaris Sarana dan Prasarana di SMK</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="peminjaman.php">Peminjaman
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="pengembalian.php">Pengembalian</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="logout.php">Logout</a>
            </li>
          
            <li class="nav-item">
              <a class="nav-link"><font color="white"><i class="fa fa-user fa-fw"></i> <?php echo $_SESSION['nama_petugas'];?></font></a>
            </li>
			</ul>
			</div>
     </div>
 </nav>
    <!-- Page Content -->
    <div class="container">
      <!-- Page Heading -->
      <h1 class="my-4"><p align="center">Peminjaman</p></h1>
      <div class="row">
	  <div class="col-lg-12">
						<p align="right">
								<a href="pinjam.php" ><button type="button" class="btn btn-outline btn-primary fa fa-plus"> Pinjam</button></a>
						</p><br/>
						<!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="example">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Tanggal Pinjam</th>
                                                    <th>Status Peminjaman</th>
                                                    <th>Nama Pegawai</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <?php
								include "koneksi.php";
								$no=1;
								$select=mysqli_query($conn,"select * from peminjaman a left join pegawai b on b.id_pegawai=a.id_pegawai where status_peminjaman='Dipinjam'");
								while($data=mysqli_fetch_array($select))
										{
								?>
                        
                                <tr class="success">
                                    <td><?php echo $no++; ?></td>
									<td><?php echo $data['tanggal_pinjam'] ?></td>
									<td><?php echo $data['status_peminjaman'] ?></td>
									<td><?php echo $data['nama_pegawai'] ?></td>
									<td>
										<a href="detail_pinjam.php?id_peminjaman=<?php echo $data['id_peminjaman']; ?>"><button type="button" class="btn btn-success">Lihat </button></a>
									</td>
                                </tr>
								<?php } ?>
                                            
                                            </tbody>
                                        </table>
									<script type ="text/javascript" src="../assets/js/jquery.min.js"></script>
									<script type ="text/javascript" src="../assets/js/jquery.dataTables.min.js"></script>
									<script>$(document).ready(function(){
											$('#example').DataTable();
											});
									</script>
                                    </div>
                                    <!-- /.table-responsive -->
                                   </div>
                                <!-- /.panel-body -->
      </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container -->

    <!-- Footer --><br/><br/><br/><br/><br/><br/><br/><br/>
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Inventaris Sarana dan Prasarana di SMK 2019</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
	<script type ="text/javascript" src="vendor/bootstrap/js/jquery.min.js"></script>
						<script type ="text/javascript" src="vendor/bootstrap/js/jquery.dataTables.min.js"></script>
						<script>$(document).ready(function(){
							$('#example').DataTable();
						});
						</script>
  </body>

</html>
