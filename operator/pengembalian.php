<?php
include('cek.php');
error_reporting(0);
?>
<?php
include "koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pengembalian Barang | Operator</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/3-col-portfolio.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../assets/css/jquery.dataTables.css">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="peminjaman.php">Inventaris Sarana dan Prasarana di SMK</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
              <a class="nav-link" href="peminjaman.php">Peminjaman
              </a>
            </li>
            <li class="nav-item">
            <li class="nav-item active">
              <a class="nav-link" href="pengembalian.php">Pengembalian
                <span class="sr-only">(current)</span>
			  </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="logout.php">Logout</a>
            </li>
            <li class="nav-item">
              <a class="nav-link"><font color="white"><i class="fa fa-user fa-fw"></i> <?php echo $_SESSION['nama_petugas'];?></font></a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">
      <!-- Page Heading -->
      <h1 class="my-4"><p align="center">Pengembalian</p></h1>
     <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-12">
							<div class="panel panel-default">
                                <div class="panel-heading">
                                    Pengembalian 
                                </div><br/>
								<div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <form method="post">
											   <select name="id_peminjaman" class="form-control m-bot15">
													<?php
													include "koneksi.php";
															//display values in combobox/dropdown
													$result = mysqli_query($conn, "SELECT id_peminjaman,kode_peminjaman from peminjaman where status_peminjaman='Dipinjam' ");
													while($row = mysqli_fetch_assoc($result))
													{
														echo "<option value='$row[id_peminjaman]'>$row[kode_peminjaman]</option>";
													} 
													?>
											   </select><br/>
													<div class="form-group">
														<input class="btn btn-primary" type="submit" name="pilih" value="Tampilkan" />
													</div>
                                            </form>
                                        </div>
                                        <!-- /.col-lg-6 (nested) -->
                                    </div>
                                    <!-- /.row (nested) -->
                                </div><br/>
								<?php if(isset($_POST['pilih'])){?>
								<form action="proses_pengembalian.php" method="post" enctype="multipart/form-data">
								<?php
									include "koneksi.php";
									$id_peminjaman=$_POST['id_peminjaman'];
									$select=mysqli_query($conn,"select * from peminjaman a left join detail_pinjam b on b.id_detail_pinjam=a.id_peminjaman
																left join inventaris c on b.id_inventaris=c.id_inventaris
																left join pegawai d on a.id_pegawai=d.id_pegawai
																where id_peminjaman='$id_peminjaman'");
									while($data=mysqli_fetch_array($select))  {
								?>
                        
                                <div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" >
										Nama Barang
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input name="id_peminjaman" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $data['id_peminjaman'];?>" autocomplete="off" maxlength="11" required="">
										<input name="id_detail_pinjam" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $data['id_detail_pinjam'];?>" autocomplete="off" maxlength="11" required="">
										<input name="id_inventaris" type="text" class="form-control col-md-7 col-xs-12" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['id_inventaris'];?>.<?php echo $data['nama'];?>" autocomplete="off" maxlength="11" required="" readonly>
									</div>
								</div>   
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" >
										Tanggal Pinjam
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="datetime" name="" value="<?php echo $data['tanggal_pinjam']?>" class="form-control col-md-7 col-xs-12" disabled placeholder="tanggal_pinjam">
										<input type="hidden" name="tanggal_pinjam" value="<?php echo $data['tanggal_pinjam']?>" >
										<input name="tanggal_kembali" type="hidden" required>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" >
										Nama Pegawai
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="text" name="" value="<?php echo $data['id_pegawai']?>.<?php echo $data['nama_pegawai']?>" class="form-control col-md-7 col-xs-12" disabled placeholder="ID Pegawai">
										<input type="hidden" name="id_pegawai" value="<?php echo $data['id_pegawai']?>" >
									</div>
								</div>
							  <div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" >
									Jumlah Pinjam
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<input type="number" name="jumlah_pinjam" value="<?php echo $data['jumlah_pinjam']?>" class="form-control col-md-7 col-xs-12"  placeholder="Jumlah" readonly>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" >
								 Status Peminjaman
							 </label>
							 <div class="col-md-6 col-sm-6 col-xs-12">
							  <select name="status_peminjaman" class="form-control col-md-7 col-xs-12" class="form-control" readonly>
								<option>Dikembalikan</option>
							</select>
						</div><br/>
						<div class="form-group">
							<button type="submit" class="btn btn-primary" /> Simpan</button>
						</div>
					</div>
				<?php } ?>
			</form>
			<?php } ?><br/>
								<div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="example">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Tanggal Pinjam</th>
                                                    <th>Tanggal Kembali</th>
                                                    <th>Status Peminjaman</th>
                                                    <th>Nama Pegawai</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <?php
								include "koneksi.php";
								$no=1;
								$select=mysqli_query($conn,"select * from peminjaman a left join pegawai c on c.id_pegawai=a.id_pegawai where status_peminjaman='Dikembalikan'");
								while($data=mysqli_fetch_array($select))
										{
								?>
                        
                                <tr class="success">
									<td><?php echo $no++; ?></td>
                                    <td><?php echo $data['tanggal_pinjam'] ?></td>
									<td><?php echo $data['tanggal_kembali'] ?></td>
									<td><?php echo $data['status_peminjaman'] ?></td>
									<td><?php echo $data['nama_pegawai'] ?></td>
                                </tr>
								<?php } ?>
                                            
                                            </tbody>
                                        </table>
									<script type ="text/javascript" src="assets/js/jquery.min.js"></script>
									<script type ="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
									<script>$(document).ready(function(){
											$('#example').DataTable();
											});
									</script>
                                    </div>
                                    <!-- /.table-responsive -->
                                   </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
    </div>
    <!-- /.container -->

    <!-- Footer --><br/><br/><br/><br/>
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Inventaris Sarana dan Prasarana di SMK 2019</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
	<script type ="text/javascript" src="vendor/bootstrap/js/jquery.min.js"></script>
						<script type ="text/javascript" src="vendor/bootstrap/js/jquery.dataTables.min.js"></script>
						<script>$(document).ready(function(){
							$('#example').DataTable();
						});
						</script>

  </body>

</html>
