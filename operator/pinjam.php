<?php
include('cek.php');
error_reporting(0);
?>
<?php
include "koneksi.php";
?>
<?php
include "koneksi.php";
$query = "SELECT max(kode_peminjaman) as maxKode FROM peminjaman";
$hasil = mysqli_query($conn,$query);
$data = mysqli_fetch_array($hasil);
$kode_peminjaman = $data['maxKode'];
$noUrut = (int) substr($kode_peminjaman, 4, 4);
$noUrut++;
$char = "PJM";
$kode_peminjaman = $char . sprintf("%04s", $noUrut);
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> Pinjam Barang | Operator</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/3-col-portfolio.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../assets/css/jquery.dataTables.css">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="peminjaman.php">Inventaris Sarana dan Prasarana di SMK</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="peminjaman.php">Peminjaman
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="pengembalian.php">Pengembalian</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="logout.php">Logout</a>
            </li>
            <li class="nav-item">
              <a class="nav-link"><font color="white"><i class="fa fa-user fa-fw"></i> <?php echo $_SESSION['nama_petugas'];?></font></a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">
      <!-- Page Heading -->
      <h1 class="my-4"><p align="center">Input Peminjaman</p></h1>
      <div class="row">
	  <div class="col-lg-12">
						<p align="right">
								<a href="peminjaman.php" ><button type="button" class="btn btn-outline btn-primary fa fa-plus"> Kembali</button></a>
						</p><br/>
						<!-- /.panel-heading -->
                               <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-8">
										<?php
											$select_inventaris   =  mysqli_query($conn, "SELECT `id_inventaris`,`nama`,`kode_inventaris`,`jumlah` FROM `inventaris`");
											$select_pegawai      =  mysqli_query($conn, "SELECT `id_pegawai`,`nip`,`nama_pegawai` FROM `pegawai`");
										?>
                                            <form action="simpan_pinjam.php" method="post" role="form" >
											 <div class="form-group">
                                                    <label>Kode Peminjaman</label>
                                                    <input class="form-control" type="text" name="kode_peminjaman" value="<?php echo $kode_peminjaman;?>" readonly>
                                              </div>
											  <div class="form-group">
												<label>Nama Barang</label>
												<select class="form-control span8" name="id_inventaris" required>
												<option value="">--- Masukan Barang ---</option>
														<?php while($data_inventaris=mysqli_fetch_array($select_inventaris)){ ?>
												  <option value="<?php echo $data_inventaris['id_inventaris']; ?>"><?php echo $data_inventaris['kode_inventaris']; ?> - <?php echo $data_inventaris['nama']; ?> | Stock ( <?php echo $data_inventaris['jumlah']; ?> )
												  </option>
														<?php } ?>
												</select>
											  </div>
											  <div class="form-group">
                                                    <label>Jumlah Pinjam</label>
                                                    <input class="form-control" type="number" name="jumlah" placeholder="Masukan Jumlah Peminjaman Barang" required>
                                              </div>
											  
                                                    <input class="form-control" name="tanggal_kembali" type="hidden" required>
                                  
											  
											  <div class="form-group">
												<label>ID Pegawai</label>
												<select class="form-control span8" name="id_pegawai" required>
												<option value="">--- Masukan Nama Pegawai ---</option>
														<?php while($data_pegawai=mysqli_fetch_array($select_pegawai)){ ?>
												  <option value="<?php echo $data_pegawai['id_pegawai']; ?>"><?php echo $data_pegawai['nip']; ?> - <?php echo $data_pegawai['nama_pegawai']; ?>
												  </option>
														<?php } ?>
												</select>
											  </div>
													<div class="form-group">
														<input class="btn btn-primary" type="submit" name="submit" value="Simpan" />
														<input class="btn btn-danger" type="reset" name="reset" value="Reset" />
													</div>
                                            </form>
                                        </div>
                                        <!-- /.col-lg-6 (nested) -->
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                <!-- /.panel-heading -->
      </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Inventaris Sarana dan Prasarana di SMK 2019</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
