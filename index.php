<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Dashboard</title>

        <!-- Bootstrap Core CSS -->
        <link href="admin/../css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="admin/../css/metisMenu.min.css" rel="stylesheet">

        <!-- DataTables CSS -->
        <link href="admin/../css/dataTables/dataTables.bootstrap.css" rel="stylesheet">

        <!-- DataTables Responsive CSS -->
        <link href="admin/../css/dataTables/dataTables.responsive.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="admin/../css/startmin.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="admin/../css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <ul class="nav navbar-right navbar-top-links">
                     <li><a href="login.php"><font color="white"><i class="fa fa-sign-in fa-fw"></i> Login</font></a></li>
                </ul>
                <!-- /.navbar-top-links -->
         
				</div>				
            <!-- /.row -->
			<div class="container">
			<div class="col-lg-12">
                                <div class="panel-heading">
                                    Dashboard 
                                </div>
			<div class="panel-body">
			 <tbody>
                                                 <?php
									include "admin/koneksi.php";
									$no=1;
									$select=mysqli_query($conn, "SELECT * FROM dashboard");
									while($data=mysqli_fetch_array($select))
									{
								?>
                        
                                <tr class="success">
									<p align="center"><?php echo $data['deskripsi'] ?></p>
                                </tr>
								<?php } ?>
                                            
                                            </tbody>
			</div>
			</div>
			</div>
        <!-- /#wrapper -->
		
		
        <!-- jQuery -->
        <script src="admin/../js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="admin/../js/bootstrap.min.js"></script>
		
        <!-- Metis Menu Plugin JavaScript -->
        <script src="admin/../js/metisMenu.min.js"></script>

        <!-- DataTables JavaScript -->
        <script src="admin/../js/dataTables/jquery.dataTables.min.js"></script>
        <script src="admin/../js/dataTables/dataTables.bootstrap.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="admin/../js/startmin.js"></script>

    </body>
</html>